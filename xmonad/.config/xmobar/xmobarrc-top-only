-- -*- mode: haskell; -*- vim: ft=haskell

Config { font = "xft:KoHo:size=12:medium:antialias=true"
       -- Choose a monospace font as the 1st one of this array
       , additionalFonts = [ "xft:Iosevka:pixelsize=13" ]
    , bgColor = "#0C0C14" --"#1d1f21" --"#1c1c1c"
        , fgColor = "#CEDBE5"
        , position = TopW C 100 -- use this if only using one monitor
        -- Both lowerOnStart and overrideRedirect are True because
        -- xmonad handles the geometry properly with avoidStruts
        , lowerOnStart = True
        , overrideRedirect = True
        , allDesktops = True
        , sepChar = "%"
        , alignSep = "}{"
        , iconRoot = "/home/gagbo/.xmonad/xpm/"  -- default: "."
        , template = " <icon=haskell_20.xpm/> <fc=#666666>|</fc> %UnsafeStdinReader% } <fc=#2D7AE5><icon=calendar-clock-icon_20.xpm/> %parisTime%</fc> { %alsa:default:Master% | %wlp4s0wi% | <fc=#899299>%cpu%</fc> <fc=#899299>%coretemp%</fc> | <fc=#899299>%memory%</fc> | %battery% "
        , commands = [
            Run Cpu [ "-t", "<icon=cpu_20.xpm/> <fn=1><bar> (<total>%)</fn>"
                ,"-L","3","-H","50"
                     ,"-p", "3"
                     ,"--normal","#2461B7"
                     ,"--high","#E55C50"] 10

        -- cpu core temperature monitor
            , Run CoreTemp       [ "--template" , "±<fn=1><core0></fn>°±<fn=1><core1></fn>°"
            , "--Low"      , "50"        -- units: °C
            , "--High"     , "70"        -- units: °C
            , "--low"      , "#2DACB7"
            , "--normal"   , "#99B740"
            , "--high"     , "#B74A40"
            , "-x"         , ""
            ] 50
            , Run Memory [ "-t", "<icon=memory-icon_20.xpm/> Mem <fn=1><usedbar> (<usedratio>%)</fn>", "-p", "3"] 10

            , Run DateZone "%a %d %b %H:%M:%S" "fr_FR.UTF-8" "Europe/Paris" "parisTime" 10
            , Run DynNetwork ["-L", "8"
                             , "-H", "32"
                             , "-l", "#899299"
                             , "-n", "#CEDBE5"
                             , "-h", "#E5F4FF"
                             , "-m", "3"
                             , "-t", "<dev> <icon=net_down_20.xpm/><fn=1><rx></fn> <icon=net_up_20.xpm/><fn=1><tx></fn>"] 10
            , Run Wireless "wlp4s0" ["-x", ""
                                    , "-L", "8"
                                    , "-H", "32"
                                    , "-l", "#B74A40"
                                    , "-n", "#B7892D"
                                    , "-h", "#99B740"
                                    , "-t", "<essid> <fn=1><quality>%</fn>"] 10 -- %wlp4s0wi%
            , Run UnsafeStdinReader

            -- volume indicator
            , Run Volume "default" "Master"
                 [ "-f", "▮", "-b", "▯"
                 , "-t", "<fc=#899299>Vol:</fc> <status><fc=#899299><fn=1><volume>%</fn></fc>"
                 , "--"
                 , "-C", "#C0E550", "-c", "#E55C50"] 10

            -- volume indicator
            , Run Alsa "default" "Master"
                 [ "-f", "▮", "-b", "▯"
                 , "-t", "<fc=#899299>Vol:</fc> <status><fc=#899299><fn=1><volume>%</fn></fc>"
                 , "--"
                 , "-C", "#C0E550", "-c", "#E55C50"]

            -- Battery status
            , Run Battery ["-t", "B: <acstatus> <watts> (<left> / <timeleft>)",
                          "-L", "10", "-H", "80", "-p", "3",
                          "-S", "True",
                          "--", "-O", "<fc=#39D7E5>On</fc> -", "-i", "",
                          "-L", "-15", "-H", "-5",
                          "-l", "#E55C50",
                          "-m", "#2D7AE5",
                          "-h", "#C0E550"]
                         150

            ]
}
