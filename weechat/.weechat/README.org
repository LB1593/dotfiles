#+TITLE: Weechat configuration

* Introduction

For obvious security reasons, the =sec.conf= and =irc.conf= files are not in the
dotfiles repo. Here's how to set it up again on a new machine

* Set up Freenode
#+BEGIN_SRC
/server add freenode chat.freenode.net/6697 -ssl
#+END_SRC

If you have already set up a connection to freenode, or if that command fails
with a message like irc: server "freenode" already exists, can't add it!, then
use these commands to ensure that SSL/TLS is enabled for your connection:

#+BEGIN_SRC
/set irc.server.freenode.addresses "chat.freenode.net/6697"
/set irc.server.freenode.ssl on
#+END_SRC

** SSL certificates
Set option weechat.network.gnutls_ca_file to file with certificates:

#+BEGIN_SRC
/set weechat.network.gnutls_ca_file "/etc/ssl/certs/ca-certificates.crt"
#+END_SRC
* Set up SASL

#+BEGIN_SRC
/set irc.server.freenode.sasl_mechanism PLAIN
/set irc.server.freenode.sasl_username <nickname>
/set irc.server.freenode.sasl_password <password>
/save
#+END_SRC
* Set up autojoin
In =.weechat/irc.conf=
#+BEGIN_SRC conf
[server]
freenode.autojoin = "#emacs,#guile"
#+END_SRC

In weechat directly
#+BEGIN_SRC
/set irc.server.freenode.autojoin "#emacs,#guile"
#+END_SRC
