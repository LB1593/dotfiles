# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
if test -r $HOME/miniconda3/bin/conda
    eval $HOME/miniconda3/bin/conda "shell.fish" "hook" $argv | source
end
# <<< conda initialize <<<

set -gx PATH $HOME/.cargo/bin $PATH
set -gx PATH $HOME/go/bin $PATH
set -gx GUIX_LOCPATH $HOME/.guix-profile/lib/locale
set -gx GUIX_PROFILE $HOME/.guix-profile
set -gx PATH $HOME/.guix-profile/bin $PATH
set -gx PATH $HOME/.config/guix/current/bin $PATH
set -gx GUILE_LOAD_PATH $GUIX_PROFILE/share/guile/site/3.0 $GUILE_LOAD_PATH
set -gx GUILE_LOAD_COMPILED_PATH $GUIX_PROFILE/lib/guile/3.0/site-ccache $GUIX_PROFILE/share/guile/site/3.0 $GUILE_LOAD_COMPILED_PATH

set -gx PATH $HOME/perl5/bin $PATH
set -gx PERL5LIB $HOME/perl5/lib/perl5 $PERL5LIB
set -gx PERL_LOCAL_LIB_ROOT $HOME/perl5 $PERL_LOCAL_LIB_ROOT
set -gx PERL_MB_OPT "--install_base \"$HOME/perl5\""
set -gx PERL_MM_OPT "INSTALL_BASE=$HOME/perl5"

set -gx PATH $HOME/.yarn/bin $PATH

set -gx PATH $HOME/bin $PATH
set -gx PATH $HOME/.local/bin $PATH
